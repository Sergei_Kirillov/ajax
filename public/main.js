const URL_BASE = '/users';

const getItemTemplate = (id, title) => {
    return `<li><i>${id}</i> ${title}</li>`;
};

const getListTemplate = (users, type = 'users') => {
    return `
        <ul class="${type}__list">
            ${users.map(user => getItemTemplate(user.id, user.name))}
        </ul>
    `;
};


const users = [
    {id: 1, name: 'Ivan', role: 'admin', books: [1,2]},
    {id: 2, name: 'Sergey', role: 'user', books: [3]}
];

const usersContainer = document.querySelector('.users__container');
const booksContainer = document.querySelector('.books__container');

const renderUsers = (users) => {
    usersContainer.innerHTML = getListTemplate(users);
};

const renderBooks = (books) => {
    booksContainer.innerHTML = getListTemplate(books);
};

const http = (url, callback, opts) => {
    // 1. Создаём новый объект XMLHttpRequest
    const client = new XMLHttpRequest();
    const {method = 'GET', data} = opts;
    console.log(data);

    // 2. Конфигурируем его: GET-запрос на url
    client.open(method, url, true);

    client.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    // by default
    // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    client.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    
    // 3. Отсылаем запрос
    client.send(data);

    client.onreadystatechange = function() { // (4) подписываемся на измененения статуса
        if (client.readyState !== 4) return;

        // 5. Если код ответа сервера не 200, то это ошибка
        if (client.status !== 200) {
            // обработать ошибку
            console.error(client.status + ': ' + client.statusText); // пример вывода: 404: Not Found
        } else {
            // вызываем callback
            callback(client.responseText); // responseText - текст ответа.
        }
    };
};

