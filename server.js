// Request  

// GET /?name=Sergey&age=25 HTTP 1.1
// host: www.site.com
// User-Agent: Mozilla.....
// Connection: Keep-Alive
// 

// POST / HTTP 1.1
// host: www.site.com
// User-Agent: Mozilla.....
// Connection: Keep-Alive
// Content-Type: application/x-www-form-urlencoded
// Content-Length: 40
// 
// [request body]

// Response

// HTTP /1.1 200 OK
// Date: ....
// Content-Type: text/html charset=utf-8
// Content-Length: 123
//
// <html> ...

// Response status
// 200 - OK
// 304 - NOT MODIFIED
// 401 - UNAUTHORIZED
// 403 - FORBIDDEN
// 404 - NOT FOUND

const http = require('http');
const fs = require('fs');

const PORT = 3000;

http.createServer((req, res) => {

  console.log(req.url);

  fs.readFile('./public/index.html', 'utf-8', (err, data) => {
    if (err) {
      console.log(err);
      res.writeHead(500, {'Content-Type': 'text/html'});
      res.end('<h1>Server Error</h1>');
    }
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(data);
  });

}).listen(PORT, () => {
  console.log(`Server is running on ${PORT} port`);
});
