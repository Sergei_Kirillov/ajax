const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const PORT = 3000;

let users = [
    {id: 1, name: 'Ivan', role: 'admin', books: [1, 2]},
    {id: 2, name: 'Sergey', role: 'user', books: [3]},
    {id: 3, name: 'Vasya', role: 'user', books: [1, 4]},
    {id: 4, name: 'Oleg', role: 'admin', books: [2, 3]},
    {id: 5, name: 'Marina', role: 'user', books: [3, 1, 5]},
    {id: 6, name: 'Irina', role: 'user', books: [3, 5]},
    {id: 7, name: 'Katya', role: 'user', books: [2, 3, 5, 6]}
];

let books = [
    {id: 1, title: 'JavaScript Templates'},
    {id: 2, title: 'Scalable JavaScript'},
    {id: 3, title: 'JavaScript Strong Parts'},
    {id: 4, title: 'JavaScript'},
    {id: 5, title: 'Graghic on JS'},
    {id: 6, title: 'Node JS by Examples'}
];

const createUser = (name, books, role = 'user') => ({
    id: users.length + 1,
    books,
    name,
    role
});

const createBook = title => ({
    id: books.length + 1,
    title
});

const getUsers = () => [...users];

const getBooks = userId => {
    const booksId = users.find(({id}) => id === userId).books;
    return books.filter(({id}) => booksId.includes(id));
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('./public'));

app.get('/users', (req, res) => {
    setTimeout(() => {
        const users = getUsers();
        res.json(users);
    }, Math.round(Math.random() * 500));
});

app.post('/users', (req, res) => {
    const {name, books, role} = req.body;
    if (!name || !books) {
        res.json({error: 'Переданны неверные параметры'});
    } else {
        users.push(createUser(name, books, role));
        res.json({result: 'Создан новый пользователь'});
    }
});


app.get('/books/:id', (req, res) => {
    setTimeout(() => {
        const id = parseInt(req.params.id);
        const books = getBooks(id);
        console.log(books);
        res.json(books);
    }, Math.round(Math.random() * 500));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(PORT, () => {
    console.log(`Server running on ${PORT}`);
});